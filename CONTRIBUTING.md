# Contributing to junit2rst

This document is for people wanting to contribute to the implementation of junit2rst.

A Gitlab account is recommended, which you can sign up for [here](https://gitlab.com/signup).

## Overview

We welcome

- ✔️ Feature requests
- ✔️ Bug reports
- ✔️ Suggestions
- ✔️ Ideas
- ✔️ Criticism

Please use the issue tracker or fill a Pull Request.
