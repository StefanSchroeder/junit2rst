// Copyright 2023 Stefan Schroeder 2023-11-10. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.

// junit2rst is a tiny command line tool that
// converts a JUnit XML file to a reStructuredText list table.

// Written for the CIP Civil Infrastructure Project.

// Usage:
//
//	junit2rst filename1 filename2 ... filenameN
//
// There are no options.

// The kinda-sorta spec is here
// https://raw.githubusercontent.com/testmoapp/junitxml/main/examples/junit-complete.xml
// List table docs:
// https://sublime-and-sphinx-guide.readthedocs.io/en/latest/tables.html
// https://docutils.sourceforge.io/docs/ref/rst/directives.html#list-table

package main

import (
	"compress/gzip"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type modus int

const (
	modeXML modus = iota
	modeGZ
)

// I used an actual report to generate the structs below.
// Testsuites was generated 2023-11-10 16:32:14 by https://xml-to-go.github.io/ in Ukraine.
type Testsuites struct {
	XMLName   xml.Name `xml:"testsuites"`
	Text      string   `xml:",chardata"`
	Disabled  string   `xml:"disabled,attr"`
	Errors    string   `xml:"errors,attr"`
	Failures  string   `xml:"failures,attr"`
	Tests     string   `xml:"tests,attr"`
	Time      string   `xml:"time,attr"`
	Testsuite []struct {
		Text      string `xml:",chardata"`
		Disabled  string `xml:"disabled,attr"`
		Errors    string `xml:"errors,attr"`
		Failures  string `xml:"failures,attr"`
		Name      string `xml:"name,attr"`
		Skipped   string `xml:"skipped,attr"`
		Tests     string `xml:"tests,attr"`
		Time      string `xml:"time,attr"`
		Timestamp string `xml:"timestamp,attr"`
		Testcase  []struct {
			Text      string `xml:",chardata"`
			Name      string `xml:"name,attr"`
			Timestamp string `xml:"timestamp,attr"`
			Classname string `xml:"classname,attr"`
			Time      string `xml:"time,attr"`
			Skipped   struct {
				Text    string `xml:",chardata"`
				Type    string `xml:"type,attr"`
				Message string `xml:"message,attr"`
			} `xml:"skipped"`
			Failure struct {
				Text    string `xml:",chardata"`
				Type    string `xml:"type,attr"`
				Message string `xml:"message,attr"`
			} `xml:"failure"`
		} `xml:"testcase"`
	} `xml:"testsuite"`
}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: juni2rst filename1 filename2 ... filenameN\n")
	os.Exit(2)
}

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		usage()
	}
	for _, j := range args {
		if strings.HasSuffix(j, ".xml") {
			process_file(j, modeXML)
		} else if strings.HasSuffix(j, ".gz") {
			process_file(j, modeGZ)
		}
	}
}

func process_file(f string, filemode modus) {
	inFile, err := os.Open(f)
	if err != nil {
		fmt.Println(err)
	}

	title := "Results of '" + f + "'"
	len_title := len(title)
	fmt.Println(title)
	line := strings.Repeat("-", len_title) 
	fmt.Println(line)
	defer inFile.Close()

	switch filemode {
	case modeXML:
		byteValues, _ := ioutil.ReadAll(inFile)
		process_bytes(byteValues)
	case modeGZ:
		fz, err := gzip.NewReader(inFile)
		if err != nil {
			return
		}
		defer fz.Close()
		byteValues, _ := ioutil.ReadAll(fz)
		process_bytes(byteValues)
	}

}

func process_bytes(b []byte) {
	var testsuites Testsuites
	xml.Unmarshal(b, &testsuites)

	fmt.Println("Nummer of Test cases in this file: " + testsuites.Tests)
	fmt.Println()

	// For all testsuites
	for i := 0; i < len(testsuites.Testsuite); i++ {

		this_testsuite := testsuites.Testsuite[i]
		fmt.Println()
		fmt.Println(".. list-table:: " + this_testsuite.Name)
		fmt.Println("   :widths: 15 20 10 5")
		fmt.Println("   :header-rows: 1")
		fmt.Println()
		fmt.Println("   * - Name")
		fmt.Println("     - Timestamp")
		fmt.Println("     - Classname")
		fmt.Println("     - Result")

		// For all test cases
		for j := 0; j < len(this_testsuite.Testcase); j++ {
			// A Testcase
			tc := testsuites.Testsuite[i].Testcase[j]
			fmt.Printf("   * - %s\n", tc.Name)
			fmt.Printf("     - %s\n", tc.Timestamp)
			fmt.Printf("     - %s\n", tc.Classname)
			result := "passed"
			if len(tc.Skipped.Type) > 0 {
				result = tc.Skipped.Type
			}
			if len(tc.Failure.Type) > 0 {
				result = tc.Failure.Type
			}
			fmt.Printf("     - %s\n", result)
		}
		fmt.Println()
	}
}
