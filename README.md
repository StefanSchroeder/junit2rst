# Junit2rst

This is a tiny commandline client to convert XML test case results to reStructuredText List tables.

It includes templates and glue code to convert those results to PDF using Pandoc.

This tool will strictly only convert an XML input file to reStructuredText.

## Install

	go install gitlab.com/StefanSchroeder/junit2rst/junit2rst@latest

## Usage

	junit2rst result.xml > result.rst
	junit2rst result.xml.gz > result.rst

## 

# Roadmap

This tool is by design kept simple. It could use some configurability, though.
File an issue if you like to see something specific.

# Known bugs

* None.

# Acknowledgments

I'd like to thank Chris Paterson for his support.
And Yaroslav Podorvanov for https://xml-to-go.github.io/.

# Integration with Pandoc

* The font Noto Sans must be available. It's the main font.
* The font PT Sans Narrow must be available. It's the header font.

See this file to see how the PT Sans Narrow font can be installed in environments that lack it:

https://gitlab.com/cip-project/cip-documents/-/blob/master/.gitlab-ci.yml?ref_type=heads#L11

* The tool 'pandoc' must be available to generate the PDF.
* A XeLaTeX environment (e.g. TeXlive) must be available to generate the PDF.

All these notes are relevant ONLY for the PDF generation.

# Copyright

(C) Copyright Stefan Schröder

