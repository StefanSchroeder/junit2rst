#!/bin/bash
# This script generates PDF documents from JUnit XML files
# using Pandoc and xeLaTeX.
# This example is only a show-case.
# Written by Stefan Schroeder for the CIP project.
# (C) CIP project 2022

TEMPLATE=../data/pandoc-template.tex
METADATA=../data/pandoc-metadata.yml
ENGINE=xelatex

echo "$0: Starting"
mkdir -p output/

cp data/*.xml data/*.png output/

pushd output/
for i in *.xml ; do
	echo "============================"
	echo "-- $i : Starting"
	pdf=${i%.xml}.pdf
	./junit2rst $i > ${i}.rst

	pandoc ${i}.rst --from rst --template=$TEMPLATE --pdf-engine=$ENGINE --metadata-file=$METADATA -o $pdf
	result="Fail"
	[ -f $pdf ] && result="Success"
	echo "-- $i : Done $result"
	echo "============================"
done
popd > /dev/null
echo "$0: Done"

